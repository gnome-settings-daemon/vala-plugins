namespace ValaDummyPlugin {

class Plugin : GnomeSettings.Plugin {
  construct {
    manager = new Manager();
  }
  
  public override void activate() {
    try {
      manager.start();
      message("Started Vala dummy plugin");
    } catch {
      warning("Could not start manager");
    }
  } 

  public override void deactivate() {
    manager.stop();
    message("Stopped Vala dummy plugin");
  }

  private Manager manager { get; set; }
}

[ModuleInit]
[CCode (cname="register_gnome_settings_plugin")]
Type register_gnome_settings_plugin(TypeModule module) {
  return typeof(Plugin);
}

} // namespace ValaDummyPlugin
