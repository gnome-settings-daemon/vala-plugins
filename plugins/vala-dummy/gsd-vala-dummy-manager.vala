namespace ValaDummyPlugin {

class Manager : GLib.Object {

  public bool start() throws GLib.Error {
    settings = new Settings("org.gnome.settings-daemon.plugins.vala-dummy");

    return true;
  }

  public bool stop() {
    return true;
  }

  private Settings settings;
}

} // namespace ValaDummyPlugin
