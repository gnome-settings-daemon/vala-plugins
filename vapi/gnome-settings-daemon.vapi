/* we'd want to call this GnomeSettingsDaemon, but then the macro
 * GNOME_SETTINGS_PLUGIN_CLASS gets named GNOME_SETTINGS_DAEMON_PLUGIN_CLASS
 * and I haven't found the attribute to prevent that
 */
namespace GnomeSettings {

[CCode (cname="GnomeSettingsPlugin", type_id="GNOME_TYPE_SETTINGS_PLUGIN",cheader_filename="gnome-settings-daemon/gnome-settings-plugin.h")]
public abstract class Plugin : GLib.Object {
  [CCode (has_construct_function = false)]
  public Plugin();
  public abstract void activate();
  public abstract void deactivate();
}

} // namespace GnomeSettingsDaemon
